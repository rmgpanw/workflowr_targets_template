---
title: "Home"
site: workflowr::wflow_site
output:
  workflowr::wflow_html:
    toc: false
    code_folding: hide
editor_options:
  chunk_output_type: console
---

Welcome to my research website.

# Target dependencies

```{r}
# for demonstration
targets::tar_load(summary)
```

# Manuscript

- Manuscript text
  - [html](manuscript.nb.html){target="_blank"}
  - [MS Word](manuscript.docx){target="_blank"}
- Manuscript tables
  - [html](tables.html){target="_blank"}
  - [MS Word](tables.docx){target="_blank"}
- Manuscript figures
  - [html](figures.html){target="_blank"}
  - [pdf](figures.pdf){target="_blank"}
  
# Targets

```{r}
targets::tar_visnetwork()
```

