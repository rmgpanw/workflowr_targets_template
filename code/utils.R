# Manuscript utils --------------------------------------------------------

bold_cap <- function(x) {
  # to be used with captioner, e.g. bold_cap(caps$main_figure('crp-replication-heatmap-cis-v-trans'))

  # bolds 'figure'/'table'
  x <- paste0("**", x)

  x <- stringr::str_replace(string = x,
                            pattern = ":",
                            replacement = ".**")

  return(x)
}


# Target factories --------------------------------------------------------

tar_render_workflowr <- function(output_dir = "public",
                                 workflowr_dir = "analysis",
                                 target_prefix = "WFLWR_",
                                 include = NULL,
                                 exclude = NULL,
                                 include_pattern = "\\.Rmd$",
                                 exclude_pattern = "^_",
                                 verbose = FALSE) {

  # workflowr files to be knitted
  wflwr_rmds <- list.files(workflowr_dir)

  # include/exclude by pattern first
  if (!is.null(include_pattern)) {
    wflwr_rmds <- subset(wflwr_rmds,
                         stringr::str_detect(wflwr_rmds,
                                             pattern = include_pattern))
  }

  if (!is.null(exclude_pattern)) {
    wflwr_rmds <- subset(
      wflwr_rmds,
      stringr::str_detect(wflwr_rmds,
                          pattern = exclude_pattern,
                          negate = TRUE)
    )
  }

  # ...include/exclude specific files
  if (!is.null(include)) {
    wflwr_rmds <- subset(wflwr_rmds,
                         wflwr_rmds %in% include)
  }

  if (!is.null(exclude)) {
    wflwr_rmds <- subset(wflwr_rmds,!wflwr_rmds %in% exclude)
  }

  # list files to be knitted
  if (verbose) {
    message(paste0("Attempting to create targets for the following workflowr files: ",
                   stringr::str_c(wflwr_rmds,
                                  sep = "",
                                  collapse = ", "),
                   "."))
  }

  # parameters to loop through
  params <- data.frame(
    rmd_filename = wflwr_rmds,
    workflowr_dir = workflowr_dir,
    output_dir = output_dir,
    target_prefix = target_prefix
  )

  # return a list of targets
  purrr::pmap(params,
              tar_render_workflowr_single)
}

tar_render_workflowr_single <- function(rmd_filename,
                                 workflowr_dir = "analysis",
                                 output_dir = "public",
                                 target_prefix = "WFLWR_") {

  # check that input Rmd file exists
  input_rmd_filepath <- file.path(workflowr_dir, rmd_filename)

  assertthat::assert_that(file.exists(input_rmd_filepath),
                          msg = paste0("No file exists at ",
                                       input_rmd_filepath))

  # html output must go to either `docs` (github) or `public` (gitlab) dir
  match.arg(output_dir,
            choices = c("public",
                        "docs"))

  # names
  html_filename <- stringr::str_replace(rmd_filename,
                                        pattern = "Rmd$",
                                        replacement = "html")
  output_html_filepath <- file.path(output_dir, html_filename)

  target_name <- toupper(rmd_filename)
  target_name <- stringr::str_replace_all(target_name,
                                          "\\.",
                                          "_")
  target_name <- paste0(target_prefix,
                       target_name)

  # return a target
  targets::tar_target_raw(
    target_name,
    command = substitute({
      suppressMessages(workflowr::wflow_build(input_rmd_filepath,
                                              verbose = FALSE))

      # returns file paths to both input Rmd and knitted output html
      c(input_rmd_filepath,
        output_html_filepath)
    }),
    deps = tarchetypes::tar_knitr_deps(input_rmd_filepath),
    format = "file"
  )
}
